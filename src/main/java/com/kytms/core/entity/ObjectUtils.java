package com.kytms.core.entity;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 简单工具类
 *
 * @author 臧英明
 * @create 2017-11-26
 */
public abstract class ObjectUtils {
    public static boolean isEmpty(Object object) {
        if (object == null) {
            return (true);
        }
        if (object.equals("")) {
            return (true);
        }
        if (object.equals("null")) {
            return (true);
        }
        return (false);
    }

    public static boolean isNotEmpty(Object object) {
        return !isEmpty(object);
    }
}
